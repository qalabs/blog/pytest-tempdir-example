import os
import pytest
import tempfile


@pytest.fixture()
def temp_dir():
    with tempfile.TemporaryDirectory() as td:
        yield td


def test_temp_dir(temp_dir):
    assert os.path.isdir(temp_dir)
